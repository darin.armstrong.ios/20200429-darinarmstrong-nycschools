//
//  SchoolListViewModelSpec.swift
//  20200429-DarinArmstrong-NYCSchoolsTests
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import XCTest

class SchoolListViewModelSpec: XCTestCase {

    let JSONData = "[{\"dbn\":\"01M292\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\"},{\"dbn\":\"01M448\",\"school_name\":\"UNIVERSITY NEIGHBORHOOD HIGH SCHOOL\",\"num_of_sat_test_takers\":\"91\",\"sat_critical_reading_avg_score\":\"383\",\"sat_math_avg_score\":\"423\",\"sat_writing_avg_score\":\"366\"}]".data(using: .utf8)
    
    let invalidJSONData = "[]".data(using: .utf8)
    
    override func setUp() {
        
    }

    func testSchoolCount() {
        guard let JSONData = JSONData,
            let invalidJSONData = invalidJSONData else {
                XCTFail()
                return
        }
        
        do {
            let viewModel = SchoolListViewModel()
            
            let invalidSchools = try JSONDecoder().decode([School].self, from: invalidJSONData)
            viewModel.schoolList = invalidSchools
            XCTAssertEqual(viewModel.schoolCount, 0)
            
            let validSchools = try JSONDecoder().decode([School].self, from: JSONData)
            viewModel.schoolList = validSchools
            XCTAssertEqual(viewModel.schoolCount, 2)
        } catch {
            XCTFail()
        }
    }
    
    func testSchoolAt() {
        guard let JSONData = JSONData,
            let invalidJSONData = invalidJSONData else {
                XCTFail()
                return
        }
        
        do {
            let viewModel = SchoolListViewModel()
            
            let invalidSchools = try JSONDecoder().decode([School].self, from: invalidJSONData)
            viewModel.schoolList = invalidSchools
            XCTAssertNil(viewModel.school(at: 0))
            
            let validSchools = try JSONDecoder().decode([School].self, from: JSONData)
            viewModel.schoolList = validSchools
            XCTAssertNotNil(viewModel.school(at: 0))
            XCTAssertNotNil(viewModel.school(at: 1))
            XCTAssertNil(viewModel.school(at: 2))
        } catch {
            XCTFail()
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
