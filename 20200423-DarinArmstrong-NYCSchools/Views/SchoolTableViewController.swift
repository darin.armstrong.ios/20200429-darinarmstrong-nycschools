//
//  SchoolTableViewController.swift
//  20200429-DarinArmstrong-NYCSchools
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import UIKit

class SchoolTableViewController: UITableViewController {

    let viewModal = SchoolListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModal.load { [weak self] in
            self?.tableView.reloadData()
        }
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let schoolDetailViewController = segue.destination as? SchoolDetailViewController,
            let item = tableView.indexPathForSelectedRow?.row,
            let school = viewModal.school(at: item) else { return }
        
        schoolDetailViewController.viewModal = SchoolDetailViewModel(school: school)
        
    }
    

}

// MARK: - Table view data source
extension SchoolTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModal.schoolCount
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell", for: indexPath) as! SchoolTableViewCell
        
        if let school = viewModal.school(at: indexPath.row) {
            cell.update(with: school)
        }

        return cell
    }
}
