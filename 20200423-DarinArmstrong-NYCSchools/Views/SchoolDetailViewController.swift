//
//  SchoolDetailViewController.swift
//  20200429-DarinArmstrong-NYCSchools
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var satTakersLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    
    
    var viewModal: SchoolDetailViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let viewModal = viewModal {
            schoolNameLabel.text = viewModal.schoolName
            satTakersLabel.text = "Total SAT Takers: \(viewModal.satTakers)"
            readingScoreLabel.text = viewModal.criticalReading
            mathScoreLabel.text = viewModal.math
            writingScoreLabel.text = viewModal.writing
        }
    }
    
    
}
