//
//  SchoolTableViewCell.swift
//  20200429-DarinArmstrong-NYCSchools
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var dbnLabel: UILabel!
    @IBOutlet private weak var schoolNameLabel: UILabel!
    
    func update(with school: School) {
        dbnLabel.text = school.dbn
        schoolNameLabel.text = school.schoolName
    }
}
