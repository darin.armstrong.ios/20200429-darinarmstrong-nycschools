//
//  NetworkingService.swift
//  20200429-DarinArmstrong-NYCSchools
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import Foundation

class NetworkingService {
    
    let schoolsURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    func downloadData(with completion: @escaping ([School]) -> Void) {
        guard let url = URL(string: schoolsURL) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            
            // Check for errors
            if let error = error {
                print("Error: \(error.localizedDescription)")
                return
            }
            
            self?.parseData(data: data, completion: completion)
            
        }
        
        task.resume()
    }
    
    func parseData(data: Data?, completion: @escaping ([School]) -> Void) {
        if let data = data {
            
            do {
                let schools = try JSONDecoder().decode([School].self, from: data)
                DispatchQueue.main.async {
                    completion(schools)
                }
            } catch {
                print("Error: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    completion([])
                }
            }
        }
    }
    
}
