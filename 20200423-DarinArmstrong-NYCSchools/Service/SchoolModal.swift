//
//  SchoolModal.swift
//  20200429-DarinArmstrong-NYCSchools
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import Foundation

struct School: Codable {
    var dbn: String?
    var schoolName: String?
    var satTakersCount: String?
    var criticalReadingAvgScore: String?
    var mathAvgScore: String?
    var writingAvgScore: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case satTakersCount = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}
