//
//  SchoolDetailViewModel.swift
//  20200429-DarinArmstrong-NYCSchools
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import Foundation

class SchoolDetailViewModel {
    
    private var school: School
    
    init(school: School) {
        self.school = school
    }
    
    var schoolName: String {
        return school.schoolName ?? ""
    }
    
    var satTakers: String {
        return school.satTakersCount ?? "0"
    }
    
    var criticalReading: String {
        return school.criticalReadingAvgScore ?? "0"
    }
    
    var math: String {
        return school.mathAvgScore ?? "0"
    }
    
    
    var writing: String {
        return school.writingAvgScore ?? "0"
    }
}
