//
//  SchoolListViewModel.swift
//  20200429-DarinArmstrong-NYCSchools
//
//  Created by Darin on 4/29/20.
//  Copyright © 2020 Darin. All rights reserved.
//

import Foundation

class SchoolListViewModel {
    let service = NetworkingService()
    var schoolList: [School] = []
    
    func load(completion: @escaping () -> Void) {
        service.downloadData { [weak self] in
            self?.schoolList = $0
            completion()
        }
    }
    
    var schoolCount: Int {
        return schoolList.count
    }
    
    func school(at index: Int) -> School? {
        guard (0..<schoolList.count).contains(index) else { return nil }
        return schoolList[index]
    }
}
